<?php

require_once(__DIR__.'/../ReviewPage.php');


class ReviewPageTest extends PHPUnit_Framework_TestCase{

	protected function setUp()
    {
        $this->reviewPage = new ReviewPage();
		$this->reviewPage->setLink("http://www.amazon.com/reviews/iframe?akid=AKIAJHMNM3BYMY5DESNA&alinkCode=xm2&asin=B001CZJBKC&atag=peblitnedespa-20&exp=2015-01-31T22%3A17%3A09Z&v=2&sig=SEGHzyevHmlJCjl5qATNm66Dbjq7PaZddwW0Ivt12fg%3D");
		$this->reviewPage->request();
    }

	public function testGetReviewStar(){
		$this->assertEquals($this->reviewPage->getReviewStar(),"4.2");
		// $foo = true;
  //   	$this->assertTrue($foo);
	}

	public function testGetReviewNum(){
		$this->assertEquals($this->reviewPage->getReviewNum(),"684");
	}
}


?>