#!/bin/bash
# Program: amazon sports product tracker
# 02/04/2015 yzhao first release
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

cd /home/yzhao/amazon-tracker/nodejs-code;
node get_root_category_rank_page.js;
node get_list_page.js;
node get_asin_from_bestseller_page.js>../public/asin_raw.txt;
node get_asin.js >> ../public/asin_raw.txt;
sort ../public/asin_raw.txt | uniq > ../public/asin.txt;
cd ../php-code;
php -d display_errors index.php;
