var cheerio = require('cheerio'),
	fs    = require('fs');

var page_directory = 'bestseller-pages/';
var asin_file = 'asin.txt';

fs.readdir(page_directory, function(err, files){
	files.forEach(function(element){
		get_asin_from_file(element);
	});
});

function get_asin_from_file(filename){
	var content = fs.readFileSync(page_directory + filename, 'utf8');
	var $ = cheerio.load(content);
	$('.zg_itemImmersion').each(function(){
		var review_block = $(this).find('.asinReviewsSummary');
		// var href = $(this).attr('href').trim();
		// var asin = href.match(/\/dp\/(.*)\/ref/)[1];
		if (review_block.length == 0 )return;
		else{
			var asin =review_block.attr('name');
			var review_star = review_block.find('.swSprite').text();
			review_star = review_star ? review_star.match(/\d+.\d+|\d+/)[0]: 0;
			var review_num = review_block.next('a').text();
			review_num = review_num?review_num.trim().replace(/[^0-9]/, ''):0;
			var output = asin + '\t' + review_star + '\t' + review_num;
			console.log(output);
		}
		
	});
}