var request = require('request'),
    cheerio = require('cheerio'),
    async   = require('async'),
    winston = require('winston'),
    moment  = require('moment'),
    fs    = require('fs');

var product_directory = 'products/';
var info_file = 'info.txt';
fs.writeFileSync(info_file, '', 'utf8');
// var current_date = moment('2014-11-28 09:00:00','YYYY-MM-DD HH:MM:SS');
var current_date = moment().format('YYYY-MM-DD HH:MM:SS');
var content_array = [];
var product_files = fs.readdirSync(product_directory);
// product_files = [product_files[0],product_files[1]];

product_files.forEach(function(element){
	console.log(element);
	var asin = element.replace('page-','');
	var url = 'http://www.amazon.com/dp/' + asin;
	var body = fs.readFileSync(product_directory + element,'utf8');
		var $ = cheerio.load(body);
		var title = $('#productTitle')? $('#productTitle').text().replace('\n',' '):'N/A';
		var review_text = $('#acrCustomerReviewText')?$('#acrCustomerReviewText').text().trim():'';
		var review_num = 0;
		if (review_text){
			review_num = review_text.match(/\d+,\d+|\d+/)[0];
			review_num = review_num.replace(',','');
		}
		var review_star = $('#acrPopover').length > 0 ? $('#acrPopover').attr('title'):0;
		if (review_star){
			review_star = review_star.match(/\d+.\d+|\d+/)[0];
		}
		var image_url = $('#landingImage').data('a-dynamic-image')? Object.keys($('#landingImage').data('a-dynamic-image'))[0]:'';
		var max_price, min_price;
		max_price = min_price = 0;
		var price = $('#priceblock_ourprice')?$('#priceblock_ourprice').text():'';
		var list_price_block = $('#price tr').first();
		var list_price;
		if (list_price_block){
			var list_price_info = list_price_block.find('td').text();
			if (list_price_info.indexOf("List Price") > -1){
				list_price = list_price_info.replace(/[^0-9.-]/g,'');
			}else{
				list_price = 0;
			}
		}else{
			list_price = 0;
		}
		var saleprice = $('#priceblock_saleprice')?$('#priceblock_saleprice').text():'';
		var dealprice = $('#priceblock_dealprice span')?$('#priceblock_dealprice span').first().text():'';
		if (price.length < 2){
			price = saleprice;
			if (price.length < 2){
				price = dealprice;
			}
		}
		if (price.length > 1){
				var price_tmp = price.replace(/[^0-9.-]/g,'');
				var price_array = price_tmp.split('-');
				if (price_array.length > 1){
					min_price = price_array[0];
					max_price = price_array[1];
					price = 0;
				}else{
					price = price_array[0];
				}
		}
		var rank_div = $('#SalesRank');
		var rank = 'N/A';
		var category = '';
		if (rank_div){
			rank_div.children().remove();
			var rank_text = rank_div.text().trim();
			if (rank_text.indexOf('Sports') > 0){
				rank = rank_text.match(/\d+,\d+|\d+|\d+,\d+,\d+/)[0];
				rank = rank.replace(',','');
				category = 'Sports';
			}else{
				rank = 999999;
				category = 'Others';
			}
			// .match(/\d+,\d+|\d+/)[0];
		}
		var result = {
			asin: asin,
			title: title,
			category: category,
			url: url,
			image_url: image_url,
			rank: rank,
			review_num: review_num,
			review_star: review_star,
			list_price: list_price,
			price: price,
		    lowest_price: min_price,
			highest_price: max_price,
	
		};
		console.log(result);
		var data =  asin + '\t' + 
					title + '\t'+
					category + '\t' + 
					url +'\t'+
					image_url + '\t'+
					rank + '\t' + 
					review_num + '\t' + 
					review_star+ '\t'+
					list_price +'\t'+
					price + '\t'+
					min_price + '\t' +
					max_price + '\t' + 
					current_date + '\n';
		console.log('write file ' + asin);
		fs.writeFileSync(info_file, data, {flag:'a'});
});



