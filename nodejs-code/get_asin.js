var cheerio = require('cheerio'),
	fs    = require('fs');

var page_directory = 'pages/';
var asin_file = 'asin.txt';

fs.readdir(page_directory, function(err, files){
	// console.log(files[1]);
	// get_asin_from_file(files[1]);

	files.forEach(function(element){
		get_asin_from_file(element);
	});
});

function get_asin_from_file(filename){
	var content = fs.readFileSync(page_directory + filename, 'utf8');
	var $ = cheerio.load(content);
	// console.log($('.s-result-item').length);
	$('.s-result-item').each(function(){
		var asin = $(this).data('asin');
		var review_star_text = $(this).find('.a-icon-star').text();
		var review_star = review_star_text? review_star_text.match(/\d+.\d+|\d+/)[0]: 0;
		var review_num = $(this).find('.a-spacing-top-mini .a-link-normal').text();
		review_num = review_num ? review_num.replace(/[^0-9]/, ''): 0;
		var output = asin+'\t'+review_star+'\t'+ review_num;
		console.log(output);
	});
}